package xmlbeolvasas;
public class Elem {

    private String tul1;
    private String tul2;

    public Elem(String tul1, String tul2) {
        this.tul1 = tul1;
        this.tul2 = tul2;
    }

    public String getTul1() {
        return tul1;
    }

    public String getTul2() {
        return tul2;
    }
    
    
    
}
